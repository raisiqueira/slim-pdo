<?php
/**
 * File: Pessoa.php
 * Author: Luis Alberto Concha Curay
 * Email: luvett11@gmail.com
 * Language: PHP
 * Date: 13/03/15
 * Time: 22:30
 * Project: slim
 * Copyright: 2015
 */


$app->get('/', function() use($app){
		$url = $app->urlFor('listPessoas');
		echo '<a href="'.$url.'">Listar pessoas</a>';
});


$app->get('/pessoas', function() use($app, $conn){
		$query   = $conn->prepare('SELECT * FROM tb_pessoa');
		$query->execute();
		$pessoas = $query->fetchAll(\PDO::FETCH_ASSOC);

		$app->render('pessoas_list.php', array('pessoas'=>$pessoas));
		$conn->close();
})
		->name('listPessoas');

$app->get('/addpessoa', function() use($app){
		$app->render('pessoa_add.php');
});

$app->post('/cadpessoa', function() use($app, $conn){

		if( isset($_POST)){
				$nome     = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
				$email    = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
				$telfixo  = filter_input(INPUT_POST, 'telfixo', FILTER_SANITIZE_STRING);
				$estado   = filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING);
				$endereco = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);

				$query = $conn->prepare("INSERT INTO tb_pessoa(tb_pessoa_nome,tb_pessoa_email, tb_pessoa_telFixo,tb_pessoa_estado,tb_pessoa_endereco)
	VALUES (:nome,:email,:telfixo,:estado,:endereco)");

				$query->bindParam(':nome', $nome, PDO::PARAM_STR);
				$query->bindParam(':email', $email, PDO::PARAM_STR);
				$query->bindParam(':telfixo', $telfixo, PDO::PARAM_STR);
				$query->bindParam(':estado', $estado, PDO::PARAM_STR);
				$query->bindParam(':endereco', $endereco, PDO::PARAM_STR);

				if( 	$query->execute() ){
						$app->flash('message', 'Usuario cadastrado com sucesso!');
						$conn->close();
				}else{
						$app->flash('erros', 'Houve um erro ao tentar cadastrar o Usuario!');
				}

				$app->redirect('/pessoas');
		}

});


$app->get('/pessoa/alterar/:id', function($id) use($app, $conn){

		$id = (int) $id;
		$query = $conn->prepare("SELECT * FROM tb_pessoa WHERE  id=:id");
		$query->bindParam(':id', $id, PDO::PARAM_INT);
		$query->execute();
		$pessoaEncontrada = $query->fetch(\PDO::FETCH_ASSOC);
		if( !$pessoaEncontrada ) {
				$app->halt(404,'Usuario não encontrado');
		}

		$app->render('pessoa_editar.php', array('pessoaEncontrada'=>$pessoaEncontrada));
});

$app->put('/pessoa/alterar/:id', function($id) use($app, $conn){
		$nome     = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
		$email    = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
		$telfixo  = filter_input(INPUT_POST, 'telfixo', FILTER_SANITIZE_STRING);
		$estado   = filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING);
		$endereco = filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING);
		$id       = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

		$query = $conn->prepare("UPDATE tb_pessoa SET tb_pessoa_nome = :nome ,tb_pessoa_email =:email, tb_pessoa_telFixo=:telfixo,tb_pessoa_estado=:estado,tb_pessoa_endereco=:endereco WHERE id = :id");

		$query->bindParam(':nome', $nome, PDO::PARAM_STR);
		$query->bindParam(':email', $email, PDO::PARAM_STR);
		$query->bindParam(':telfixo', $telfixo, PDO::PARAM_STR);
		$query->bindParam(':estado', $estado, PDO::PARAM_STR);
		$query->bindParam(':endereco', $endereco, PDO::PARAM_STR);
		$query->bindParam(':id', $id, PDO::PARAM_INT);

		if( 	$query->execute() ){
				$app->flash('message', 'Usuario alterado com sucesso!');
				$conn->close();
		}else{
				$app->flash('erros', 'Houve um erro ao tentar alterar o Usuario!');
		}

		$app->redirect('/pessoa/alterar/'.$id);
});

$app->get('/pessoa/deletar/:id', function($id) use($app, $conn){
		$id = (int)$id;

		$query = $conn->prepare("DELETE FROM tb_pessoa WHERE id = :id");
		$query->bindParam(':id', $id, PDO::PARAM_INT);


		if( 	$query->execute() ){
				$app->flash('message', 'Usuario DELETADO com sucesso!');
		}else{
				$app->flash('erros', 'Houve um erro ao tentar DELETAR o Usuario!');
				$conn->close();
		}

		$app->redirect('/pessoas');
});

$app->delete('/pessoa/deletar/:id', function($id) use($app, $conn){
		$id = (int)$id;
		$query = $conn->prepare("DELETE FROM tb_pessoa WHERE id = :id");
		$query->bindParam(':id', $id, PDO::PARAM_INT);

		if(	$query->execute() ){
				echo 'deletou';
		}else{
				echo 'ndeletou';
		}
		$conn->close();
});