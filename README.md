# Simple Crud Slim + PDO
##Prerequisites
* Composer

##Components
- Bootstrap v3.3.0
- Bootbox.js [v4.3.0]
- jQuery v2.1.3 
- Composer version 1.0-dev
- Slim "2.*"

##Deployment model
* Git clone local:
   git clone 
* Install dependencies:
    composer.phar install
* Server configuration (virtualhost)
```sh
    <VirtualHost *:80>
        ServerName slim.twig.com
        DocumentRoot /var/www/html/slim
        SetEnv APPLICATION_ENV "development"
        <Directory "/var/www/html/slim">
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
   </VirtualHost>
```   
 * Server configuration (embedded web server)
    cd/path_project
    127.0.0.1:9090
 
###Folder Structure and initial files and folders
* slim
    -  config
        - Connection.php
    - public
        - assets
        - css
        - js
    - routers
        - pessoa.php
    - sql
        - slim.sql
    - vendor
        - composer
        - slim
        - autoloar.php
    - views 
        - pessoa_add.php 
        - pessoa_editar.php 
        - pessoa_list.php 
    - .gitignore
    - .htaccess
    - composer.json
    - composer.phar
    - config.php
    - index.php
    - README.md
    
###Create DB
- Run slim.sql file inside the sql folder

### Version
1.0.

### Developer
- Luis Alberto Concha Curay
- Email: luisconchacuray@gmail.com
- Site: http://luisconchacuray.com.br!
