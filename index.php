<?php
/**
 * File: index.php
 * Author: Luis Alberto Concha Curay
 * Email: luvett11@gmail.com
 * Language: PHP
 * Date: 13/03/15
 * Time: 20:15
 * Project: slim
 * Copyright: 2015
 */

require_once 'config.php';

require_once ROOT . 'routers/pessoa.php';

$app->run();