<?php

namespace config;

class Conection {
		private static $instace = null;
		private static $dbType  = "mysql";
		private static $host    = "localhost";
		private static $user    = "root";
		private static $senha   = "";
		private static $db      = "estudos";

		public static function getInstance() {

				try {

						self::$instace = new \PDO(self::$dbType . ':host=' . self::$host . ';dbname=' . self::$db
								, self::$user
								, self::$senha
								, array(\PDO::ATTR_PERSISTENT => true));
						self::$instace->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
						self::$instace->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

				} catch (\PDOException $e) {
						echo 'Erro ao tentar conectar com o banco de dados: ' . $e->getMessage();
				}

				return self::$instace;
		}

		public static function prepare($sql) {
				return self::getInstance()->prepare($sql);

		}

		public static function close() {
				if (self::$instace != null) {
						self::$instace = null;
				}
		}
}