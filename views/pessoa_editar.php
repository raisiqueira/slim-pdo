<?php
/**
 * File: pessoa_add.php
 * Author: Luis Alberto Concha Curay
 * Email: luvett11@gmail.com
 * Language: PHP
 * Date: 14/03/15
 * Time: 12:00
 * Project: slim
 * Copyright: 2015
 */

?>

<!doctype html>
<html lang="pt-br">
<head>
		<meta charset="UTF-8">
		<title>pessoas</title>
		<link rel="stylesheet" href="http://<?php echo BASE_URL ?>public/assets/bootstrap/css/bootstrap.min.css"/>
</head>
<body>
<h1>Editar de  pessoas</h1>


<?php if(isset($flash['message'])): ?>
		<div role="alert" class="alert alert-success alert-dismissible fade in">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong>Aviso!</strong> <?php echo $flash['message'] ?>
		</div>
<?php endif; ?>

<?php if(isset($flash['erros'])): ?>
		<div role="alert" class="alert alert-danger alert-dismissible fade in">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong>Aviso!</strong> <?php echo $flash['erros'] ?>
		</div>
<?php endif; ?>


<div class="row">
		<div class="col-md-9" role="main">
			<div data-example-id="simple-horizontal-form" class="bs-example">
				<form class="form-horizontal" action="/pessoa/alterar/<?php echo $pessoaEncontrada['id']; ?>" method="post">
						<input type="hidden" name="id" value="<?php echo  $pessoaEncontrada['id']; ?>"/>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Nome</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o nome" id="nome" name="nome" value="<?php echo $pessoaEncontrada['tb_pessoa_nome'];?>" class="form-control">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Email</label>
								<div class="col-sm-10">
										<input type="email" placeholder="Digite o email" id="nome" name="email" value="<?php echo $pessoaEncontrada['tb_pessoa_email']; ?>" class="form-control">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Telefone fixo</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o telefone" id="nome" name="telfixo" value="<?php echo $pessoaEncontrada['tb_pessoa_telFixo']; ?>" class="form-control">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Estado</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o estado" id="nome" name="estado" value="<?php echo $pessoaEncontrada['tb_pessoa_estado']; ?>" class="form-control">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Endereço</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o endereço" id="endereco" name="endereco" value="<?php echo $pessoaEncontrada['tb_pessoa_endereco']; ?> "class="form-control">
								</div>
						</div>

						<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
										<input type="hidden" name="_METHOD" value="PUT"/>
										<button class="btn btn-primary" type="submit">Editar registro</button>
										<a href="/pessoas" class="btn btn-warning" type="submit">Listar usuários</a>
								</div>
						</div>
				</form>
			</div>
		</div>
</div>

</body>

</html>