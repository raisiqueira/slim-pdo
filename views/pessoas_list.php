<?php
/**
 * File: index.phtml
 * Author: Luis Alberto Concha Curay
 * Email: luvett11@gmail.com
 * Language: PHP
 * Date: 13/03/15
 * Time: 22:33
 * Project: slim
 * Copyright: 2015
 */
?>

<!doctype html>
<html lang="pt-br">
<head>
		<meta charset="UTF-8">
		<title>pessoas</title>
		<link rel="stylesheet" href="../public/assets/bootstrap/css/bootstrap.min.css"/>
</head>
<body>
<h1>Lista  de pessoas</h1>

<h3><a href="<?php echo 'addpessoa'?>">Novo cadastro</a></h3>

<?php if(isset($flash['message'])): ?>
		<div role="alert" class="alert alert-success alert-dismissible fade in">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong>Aviso!</strong> <?php echo $flash['message'] ?>
		</div>
<?php endif; ?>

<?php if(isset($flash['erros'])): ?>
		<div role="alert" class="alert alert-danger alert-dismissible fade in">
				<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>
				<strong>Aviso!</strong> <?php echo $flash['erros'] ?>
		</div>
<?php endif; ?>

<table class="table table-hover" id="tblPessoas">
		<tr>
				<td>Nome</td>
				<td>Email</td>
				<td>Telefone</td>
				<td>Estado</td>
				<td>Ações</td>
		</tr>
<?php foreach($pessoas as $pessoa) : ?>
		<tr>
				<td><?php echo $pessoa['tb_pessoa_nome'] ?></td>
				<td><?php echo $pessoa['tb_pessoa_email'] ?></td>
				<td><?php echo $pessoa['tb_pessoa_telFixo'] ?></td>
				<td><?php echo $pessoa['tb_pessoa_estado'] ?></td>
				<td>
						<a href="/pessoa/alterar/<?php echo $pessoa['id'] ?>">Alterar</a> -
						<a href="/pessoa/deletar/<?php echo $pessoa['id'] ?> ">Excluir </a> -
						<a href="#" id="btn_deletar" data-id="<?php echo $pessoa['id'] ?>">Excluir com javascript</a>

				</td>
		</tr>

<?php 	endforeach; ?>
</table>

</body>

<script src="../public/js/jquery.js"></script>
<script src="../public/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../public/assets/bootbox/bootbox.js"></script>
<script src="../public/js/pessoa.js"></script>
</html>