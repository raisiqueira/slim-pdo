<?php
/**
 * File: pessoa_add.php
 * Author: Luis Alberto Concha Curay
 * Email: luvett11@gmail.com
 * Language: PHP
 * Date: 14/03/15
 * Time: 12:00
 * Project: slim
 * Copyright: 2015
 */
?>

<!doctype html>
<html lang="pt-br">
<head>
		<meta charset="UTF-8">
		<title>pessoas</title>
		<link rel="stylesheet" href="../public/assets/bootstrap/css/bootstrap.min.css"/>
</head>
<body>
<h1>Cadastro de  pessoas</h1>

<div class="row">
		<div class="col-md-9" role="main">
			<div data-example-id="simple-horizontal-form" class="bs-example">
				<form class="form-horizontal" action="/cadpessoa" method="post">
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Nome</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o nome" id="nome" name="nome" class="form-control" required="required">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Email</label>
								<div class="col-sm-10">
										<input type="email" placeholder="Digite o email" id="nome" name="email" class="form-control"  required="required">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Telefone fixo</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o telefone" id="nome" name="telfixo" class="form-control"  required="required">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Estado</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o estado" id="nome" name="estado" class="form-control"  required="required">
								</div>
						</div>
						<div class="form-group">
								<label class="col-sm-2 control-label" for="nome">Endereço</label>
								<div class="col-sm-10">
										<input type="text" placeholder="Digite o endereço" id="endereco" name="" class="form-control" required="required">
								</div>
						</div>

						<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
										<button class="btn btn-primary" type="submit">Salvar registro</button>
								</div>
						</div>
				</form>
			</div>
		</div>
</div>

</body>

</html>