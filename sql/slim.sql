CREATE DATABASE IF NOT EXISTS `estudos` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `estudos`;

CREATE TABLE IF NOT EXISTS `tb_pessoa` (
  `id` int(11) NOT NULL,
  `tb_pessoa_nome` varchar(200) NOT NULL,
  `tb_pessoa_email` varchar(100) DEFAULT NULL,
  `tb_pessoa_telFixo` varchar(12) DEFAULT NULL,
  `tb_pessoa_estado` varchar(100) DEFAULT NULL,
  `tb_pessoa_endereco` varchar(200) DEFAULT NULL,
  `tb_pessoa_foto` varchar(255) DEFAULT NULL,
  `tb_pessoa_avatar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

ALTER TABLE `tb_pessoa` ADD PRIMARY KEY (`id`);

INSERT INTO `tb_pessoa` (`id`, `tb_pessoa_nome`, `tb_pessoa_email`, `tb_pessoa_telFixo`, `tb_pessoa_estado`, `tb_pessoa_endereco`, `tb_pessoa_foto`, `tb_pessoa_avatar`) VALUES
  (1, 'Luis Alberto Concha Curay', 'luvett11@gmail.com', '112255555', 'bsb', 'nova casa', NULL, NULL),
  (4, 'Alberto Bengoleia', 'albert@gmail.com', '1231231', 'Sao Paulo', 'casa do alberto', NULL, NULL);